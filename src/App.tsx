import React, {useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';

function requestToBack() {
    const url = process.env.REACT_APP_BACKEND_URL + '/api/users';
    return fetch(url)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            return data;
        });
}

function App() {
    const [data, setData] = useState(0);

    useEffect(() => {
        const fetchData = async () => {
            const result = await requestToBack();
            result['hydra:totalItems'] ? setData(result['hydra:totalItems']) : setData(-1);
        };

        fetchData();
    }, []);

    const getUsersInfo = () => {
        if (data === -1) {
            return 'Can not get data about users';
        }

        return 'Users in database: ' + data;
    };

    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                <p>
                    Edit <code>src/App.tsx</code> and save to reload.
                </p>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
                </a>
                <p>{getUsersInfo()}</p>
            </header>
        </div>
    );
}

export default App;
